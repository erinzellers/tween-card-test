import Vue from 'vue'
import App from './App.vue'
import {TweenMax, CSSPlugin, ScrollToPlugin, Draggable, Elastic} from "gsap/all";

new Vue({
  el: '#app',
  render: h => h(App)
})
